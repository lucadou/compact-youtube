"""
This file is part of Compact YouTube, a set of filter lists for uBlock.
Copyright (C) 2022-2024 Luna Lucadou

Compact YouTube is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Compact YouTube is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Compact YouTube.
If not, see <https://www.gnu.org/licenses/>.
"""

from typing import TypedDict


class FilterList(TypedDict):
    """
    Container for the filters associated with each option:

    :param show_all: shows icon and text
    :param show_btn: shows icon only
    :param show_none: shows neither (deletes the button entirely)
    """

    show_all: str
    show_btn: str
    show_none: str


class Button(TypedDict):
    """
    Container for a button and its associated FilterList

    :param name: a description of the button in question
    :param filters: the associated filters to modify the button's appearance
    """

    name: str
    filters: FilterList


class ButtonList(TypedDict):
    """
    Container for Button objects

    :param opts: all the Buttons this application can interact with, in a List
    """

    opts: list[Button]


class FilterInfo(TypedDict):
    """
    Intermediate container for filter combination creation

    :param name: a description of the button in question
    :param mode: the shortname of the filter
    :param filter: the associated filter to modify the button's appearance
    """

    name: str
    mode: str
    filter: str

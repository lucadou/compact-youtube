"""
This file is part of Compact YouTube, a set of filter lists for uBlock.
Copyright (C) 2022-2024 Luna Lucadou

Compact YouTube is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Compact YouTube is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Compact YouTube.
If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
import hashlib
import logging
import sys

try:
    from src_generator import constants, filters  # pylint: disable=import-error
except ModuleNotFoundError:
    import constants
    import filters

logger = logging.getLogger(__name__)
logging.basicConfig(
    level=logging.DEBUG,
    format=constants.default_log_format,
    handlers=[logging.StreamHandler(sys.stdout)],
)


def generate_header(filter_list: tuple[filters.FilterInfo, ...]) -> tuple[str, str]:
    """
    Generates the header for the filter and the name of it based on the given
    FilterInfo combination

    :param filter_list: a tuple of FilterInfo objects
    :return: a tuple containing the filename and file header
    """
    # Concatenate names and modes
    f_nm: list[str] = []
    for f_info in filter_list:
        f_nm += [f'{f_info.get("name")}={f_info.get("mode")}']
    nm_list: str = ", ".join(f_nm)

    # Generate filename
    logger.debug("Generating file name...")
    f_name: str = hashlib.md5(nm_list.encode()).hexdigest()
    logger.debug('Options "%s" will be written to "%s.txt"', nm_list, f_name)

    # Generate header
    logger.debug("Generating header...")
    f_content: str = ""
    f_content += constants.header_ln_00.format(variant=f_name)
    f_content += constants.header_ln_01.format(version=constants.version)
    f_content += constants.header_ln_02
    f_content += constants.header_ln_03
    f_content += constants.header_ln_04
    f_content += constants.header_ln_05
    f_content += constants.header_ln_06.format(info=nm_list)
    f_content += constants.header_ln_07
    f_content += constants.header_ln_08.format(
        timestamp=str(datetime.datetime.now(tz=datetime.timezone.utc))
    )
    f_content += constants.header_ln_09
    f_content += constants.header_ln_10
    return f_name, f_content


def generate_file_contents(filter_list: tuple[filters.FilterInfo, ...]) -> str:
    """
    Generates the filter file contents based on the given FilterInfo
    combination

    :param filter_list: a tuple of FilterInfo objects
    :return: the contents of the file
    """
    logger.debug("Generating file contents...")
    f_content: list[str] = []
    for f_element in filter_list:
        f_content += [f_element.get("filter")]
    f_content_str: str = "\n".join(f_content)
    return f_content_str


def build_file(filter_list: tuple[filters.FilterInfo, ...]) -> tuple[str, str]:
    """
    Generates the filter file contents and name based on the given FilterInfo
    combination

    :param filter_list: a tuple of FilterInfo objects
    :return: a tuple containing the file name and its contents
    """
    logger.debug("Building file...")
    # Generate headers
    headers: tuple[str, str] = generate_header(filter_list)

    # Generate file contents
    contents: str = generate_file_contents(filter_list)

    # Concat headers with contents
    file_contents: str = headers[1] + "\n" + contents

    # Return
    return headers[0], file_contents

"""
This file is part of Compact YouTube, a set of filter lists for uBlock.
Copyright (C) 2022-2024 Luna Lucadou

Compact YouTube is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Compact YouTube is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Compact YouTube.
If not, see <https://www.gnu.org/licenses/>.
"""

import pathlib

version: str = "4.0.3"
# ^ THIS MUST BE UPDATED WITH EVERY RELEASE! ^

default_log_format: str = "%(asctime)s %(name)s %(levelname)s - %(message)s"

# Files & Directories
src_generator_dir: str = str(pathlib.Path(__file__).parent.resolve())
repo_dir: str = str(pathlib.Path(__file__).parent.parent.resolve())
conf_file_path: str = src_generator_dir + "/list_options.json"
pub_dir: str = repo_dir + "/public"
pub_lists_dir: str = repo_dir + "/public/lists"
# Maintainer Note: If you change pub_lists_dir, make sure to update it in
# src_selector/assets/js/formChecker.js, too!
webapp_dir: str = repo_dir + "/src_selector"
webapp_node_file_dest: str = repo_dir + "/public/app.js"
webapp_index_file_dest: str = repo_dir + "/public/index.html"

# Header
header_ln_00: str = "! Title: Compact YouTube (Variant {variant})\n"
header_ln_01: str = "! Version: {version}\n"
header_ln_02: str = "! Description:\n"
header_ln_03: str = (
    "!     Allows buttons beneath the YouTube Player to be reliably displayed\n"
)
header_ln_04: str = "!     at any screen size.\n"
header_ln_05: str = "!     Variant Configuration:\n"
header_ln_06: str = "!     {info}\n"
header_ln_07: str = "! Expires: 7 days\n"
header_ln_08: str = "! Last modified: {timestamp}\n"
header_ln_09: str = "! Homepage: https://gitlab.com/lucadou/compact-youtube\n"
header_ln_10: str = (
    "! License: https://gitlab.com/lucadou/compact-youtube/-/blob/main/LICENSE.md\n"
)

# Website version stamper
version_stamp_cmd: str = f"sed -i s/%VERSION%/{version}/g {webapp_index_file_dest}"
# -i = in-place edit
# s/%VERSION%/{version}/g - replace %VERSION% with the "version" variable
# {webapp_index_file_dest} - input file

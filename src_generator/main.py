"""
This file is part of Compact YouTube, a set of filter lists for uBlock.
Copyright (C) 2022-2024 Luna Lucadou

Compact YouTube is free software: you can redistribute it and/or modify it
under the terms of the GNU General Public License as published by the Free
Software Foundation, either version 3 of the License, or (at your option) any
later version.

Compact YouTube is distributed in the hope that it will be useful, but WITHOUT
ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
FOR A PARTICULAR PURPOSE.
See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with Compact YouTube.
If not, see <https://www.gnu.org/licenses/>.
"""

import itertools
import json
import logging
import os
import shutil
import subprocess
import sys

try:
    from src_generator import (  # pylint: disable=import-error
        constants,
        filter_generator,
        filters,
    )
except ModuleNotFoundError:
    # This is ugly, yes, but it's the ONLY way to get this to work.
    # Coverage & pytest do not need this try-except, but when running
    # "python src_generator/main.py" or "cd src_generator && python main.py",
    # you *have* to have this.
    #
    # If venv would just run 'export PYTHONPATH="$PYTHONPATH:$(pwd)"' like pipenv,
    # this would be *completely* unnecessary.
    # We could add 'export PYTHONPATH="$PYTHONPATH:$(pwd)"' to venv/bin/activate,
    # but that would require putting the venv folder in version control
    # and overriding future changes to venv
    # (and modifying venv/bin/activate is considered bad practice).
    #
    # You might be wondering: Why not just use python-dotenv?
    # The problem is that, once the Python interpreter has been loaded,
    # a .env file containing a PYTHONPATH override is useless -
    # the import search paths cannot be modified without while running unless
    # you feel like messing with sys.path, which is bad practice
    # and certainly doesn't help with IDE hints or Pylint errors.
    #
    # Having a "startup script" that runs 'export PYTHONPATH="$PYTHONPATH:$(pwd)"'
    # would also fix this problem, at the cost of:
    # a) needing a weird run configuration locally and in CI,
    # b) still needing the Pylint error suppression, and
    # c) still not helping out with IDE hints.
    #
    # So the choice comes down to:
    # a) use pipenv - local imports *just work* across the entire project, BUT
    #    it cannot handle multiple Python versions
    #    (which causes major problems with libraries that change dependency versions
    #     depending on installed Python),
    #    it requires more work to set up and execute commands during CI,
    #    and, if using PyCharm, pipenv support has some annoying corner cases, or...
    # b) use venv - local imports work from some modules, like pytest,
    #    and it can be setup entirely from the stdlib,
    #    with the unfortunate downside of having to deal with this mess.
    #
    # If this application gets more complex in the future,
    # we will likely have to switch to pipenv.
    # This try-except method is only maintainable for a few files.
    import constants
    import filter_generator
    import filters

# Setup logger
logger: logging.Logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

# Setup logging to stdout
log_sh: logging.StreamHandler = logging.StreamHandler(sys.stdout)
log_sh.setLevel(logging.DEBUG)

# Setup log formatter
log_fmtr: logging.Formatter = logging.Formatter(constants.default_log_format)
log_sh.setFormatter(log_fmtr)

# Attach stdout logger
logger.addHandler(log_sh)
logger.debug("Logger setup complete")


def create_ci_dirs() -> None:
    """
    Creates temporary directories for the CI pipeline

    :return: None
    """
    logger.debug("Creating CI directories...")
    try:
        os.mkdir(constants.pub_dir)
        os.mkdir(constants.pub_lists_dir)
    except FileExistsError:
        logger.debug("CI directories already exist; ignoring")


def read_config(location: str) -> list[filters.Button]:
    """
    Reads in the specified configuration file and returns all options as
    a list of Button (TypedDict) objects.

    :param location: the location of the configuration file
    :return: the list of all Buttons and their filter options
    """
    logger.debug("Reading in configuration...")
    conf_data: filters.ButtonList
    with open(location, "r", encoding="utf-8") as conf_file:
        conf_data: filters.ButtonList = json.load(conf_file)
    conf_file.close()

    conf_list: list[filters.Button] = []
    for button in conf_data.get("opts"):
        conf_list += [button]

    return conf_list


def generate_combinations(options: list[filters.Button]) -> list[tuple[str, str]]:
    """
    Generates all possible combinations of filters, given a list of Buttons.

    Returns a list containing tuples with the name of the file and the data to
    write to disk covering every combination of Buttons and their filters.

    :param options: the list of Buttons to generate combinations for
    :return: a list of tuples containing file names and contents
    """
    logger.debug("Generating combinations...")
    # Convert Button objects to FilterInfo objects
    f_opts: list[list[filters.FilterInfo]] = []
    for opt in options:
        cur_opts: list[filters.FilterInfo] = []
        button_name: str = opt.get("name")
        for filter_name, filter_text in opt.get("filters").items():
            f_info: filters.FilterInfo = {
                "name": button_name,
                "mode": filter_name,
                "filter": filter_text,
            }
            cur_opts += [f_info]
        f_opts += [cur_opts]

    # Generate all combinations
    f_combos: list[tuple[filters.FilterInfo, ...]] = list(itertools.product(*f_opts))
    combo_files: list[tuple[str, str]] = []
    for combo in f_combos:
        combo_files += [filter_generator.build_file(combo)]

    return combo_files


def write_files(output_dir: str, files: list[tuple[str, str]]) -> None:
    """
    Given a list of filenames and their contents, writes them
    to the disk

    :param output_dir: the directory to save the files to
    :param files: the files and their contents to write to disk
    :return: None
    """
    logger.debug("Writing filter files...")
    for file_data in files:
        f_path: str = f"{output_dir}/{file_data[0]}.txt"
        logger.debug("Writing file %s", f_path)
        with open(f_path, "w", encoding="utf-8") as filter_file:
            filter_file.write(file_data[1])
        filter_file.close()
    logger.debug("File writing complete!")


def copy_webapp() -> None:
    """
    Copies the web application's files into the public directory for
    publishing on GitLab Pages

    :return: None
    """
    logger.debug("Copying web application files...")
    shutil.copytree(constants.webapp_dir, constants.pub_dir, dirs_exist_ok=True)
    os.remove(constants.webapp_node_file_dest)  # No reason to keep this around


def version_stamp_index() -> None:
    """
    Modifies the web application's index file in the public directory
    to apply the version stamp.

    :return: None
    """
    logger.debug("Applying version stamp...")
    try:
        _: subprocess.CompletedProcess or subprocess.CompletedProcess[bytes] = (
            subprocess.run(constants.version_stamp_cmd, shell=True, check=True)
        )
        logger.debug("Version stamp applied")
    except subprocess.CalledProcessError as e:
        logger.exception("Version stamping failed: %s", e, exc_info=True)


def build_filters(config_file_path: str, output_dir: str) -> None:
    """
    Orchestrates reading in the configuration file,
    generating all possible combinations,
    and writing them to the disk.

    :param config_file_path: the location of the JSON filter file
    :param output_dir: where to write the generated filter files
    :return: None
    """
    create_ci_dirs()
    data: list[filters.Button] = read_config(config_file_path)
    file_combos: list[tuple[str, str]] = generate_combinations(data)
    write_files(output_dir, file_combos)
    copy_webapp()
    version_stamp_index()


if __name__ == "__main__":
    logger.info("Starting filter builder...")
    build_filters(constants.conf_file_path, constants.pub_lists_dir)
    logger.info("Filter building complete")

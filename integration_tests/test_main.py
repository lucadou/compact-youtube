import pathlib
import shutil

import pytest

from src_generator import constants, main
from test_resources.test_helpers import helpers


@pytest.fixture(autouse=True)
def run_around_tests():
    # https://stackoverflow.com/a/22638709/15264046
    if pathlib.Path(
        str(pathlib.Path(__file__).parent.resolve()) + "/../public/"
    ).exists():
        shutil.rmtree(str(pathlib.Path(__file__).parent.resolve()) + "/../public/")


def test_build_filters_copies_website_files(helpers):
    conf_file: str = helpers.get_test_files()[0]
    main.build_filters(conf_file, constants.pub_lists_dir)
    assert pathlib.Path(
        str(pathlib.Path(__file__).parent.resolve()) + "/../public/index.html"
    ).exists()
    assert pathlib.Path(
        str(pathlib.Path(__file__).parent.resolve())
        + "/../public/assets/js/formChecker.js"
    ).exists()


def test_build_filters_no_filters(helpers):
    conf_file: str = helpers.get_test_files()[0]
    main.build_filters(conf_file, constants.pub_lists_dir)
    helpers.assert_generated_files_match_baseline(helpers.get_sample_output_paths()[0])


def test_build_filters_one_filter(helpers):
    conf_file: str = helpers.get_test_files()[1]
    main.build_filters(conf_file, constants.pub_lists_dir)
    helpers.assert_generated_files_match_baseline(helpers.get_sample_output_paths()[1])


def test_build_filters_multiple_filters(helpers):
    conf_file: str = helpers.get_test_files()[2]
    main.build_filters(conf_file, constants.pub_lists_dir)
    helpers.assert_generated_files_match_baseline(helpers.get_sample_output_paths()[2])

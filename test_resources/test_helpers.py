import filecmp
import os
import pathlib
import subprocess

import pytest

from src_generator import constants


class Helpers:
    # https://stackoverflow.com/a/42156088/15264046
    @staticmethod
    def get_public_path() -> str:
        return str(pathlib.Path(__file__).parent.resolve()) + "/../public/"

    @staticmethod
    def get_public_lists_path() -> str:
        return str(pathlib.Path(__file__).parent.resolve()) + "/../public/lists/"

    @staticmethod
    def get_sample_output_paths() -> tuple[str, ...]:
        return (
            constants.repo_dir + "/test_resources/output/conf0/",
            constants.repo_dir + "/test_resources/output/conf1/",
            constants.repo_dir + "/test_resources/output/conf2/",
        )

    @staticmethod
    def get_test_files() -> tuple[str, ...]:
        return (
            constants.repo_dir + "/test_resources/conf0.json",
            constants.repo_dir + "/test_resources/conf1.json",
            constants.repo_dir + "/test_resources/conf2.json",
        )

    @staticmethod
    def rm_timestamps_from_generated_lists() -> None:
        lists_path: str = Helpers.get_public_lists_path()
        for outfile in os.listdir(lists_path):
            outfile_path: str = lists_path + "/" + outfile
            timestamp_stripper: str = f'sed -i "/! Last modified:.*$/d" {outfile_path}'
            version_stripper: str = f'sed -i "/! Version:.*$/d" {outfile_path}'
            _: subprocess.CompletedProcess or subprocess.CompletedProcess[bytes] = (
                subprocess.run(
                    timestamp_stripper, shell=True, capture_output=True, check=True
                )
            )
            _: subprocess.CompletedProcess or subprocess.CompletedProcess[bytes] = (
                subprocess.run(
                    version_stripper, shell=True, capture_output=True, check=True
                )
            )

    @staticmethod
    def assert_generated_files_match_baseline(baseline_dir: str) -> None:
        # Clean files before comparing
        Helpers.rm_timestamps_from_generated_lists()

        # Compare directory contents
        sample_files: set[str] = set(os.listdir(baseline_dir))
        generated_files: set[str] = set(os.listdir(Helpers.get_public_lists_path()))
        assert sample_files == generated_files

        # Byte-by-byte file comparison
        for file in sample_files:
            generated_file: str = Helpers.get_public_lists_path() + "/" + file
            baseline_file: str = baseline_dir + "/" + file
            assert filecmp.cmp(generated_file, baseline_file, shallow=False)
            # https://docs.python.org/3/library/filecmp.html#filecmp.cmp
            # shallow=True - compare file names only
            # shallow=False - byte-by-byte comparison


@pytest.fixture
def helpers():
    return Helpers

! Title: Compact YouTube (Variant ac0de594f028c31ee08df1b7f1107bfd)
! Description:
!     Allows buttons beneath the YouTube Player to be reliably displayed
!     at any screen size.
!     Variant Configuration:
!     foo=show_all
! Expires: 7 days
! Homepage: https://gitlab.com/lucadou/compact-youtube
! License: https://gitlab.com/lucadou/compact-youtube/-/blob/main/LICENSE.md



import pathlib
from unittest.mock import patch, mock_open

import pytest

from src_generator import constants, filters, main
from test_resources.test_helpers import helpers


@pytest.fixture
def test_filters() -> tuple[list[filters.Button], ...]:
    return (
        [],
        [{"name": "foo", "filters": {"show_all": "\n", "show_none": "##foo\n"}}],
        [
            {"name": "foo", "filters": {"show_all": "\n"}},
            {
                "name": "bar",
                "filters": {
                    "show_all": "\n",
                    "show_btn": "##bar > span",
                    "show_none": "##bar\n",
                },
            },
            {
                "name": "baz",
                "filters": {
                    "show_all": "\n",
                    "show_btn": "\n##baz > span",
                    "show_none": "##baz\n",
                },
            },
        ],
    )


def test_create_ci_dirs():
    with patch("src_generator.main.os.mkdir") as patched_mkdir:
        main.create_ci_dirs()
        assert patched_mkdir.call_count == 2


def test_create_ci_dirs_already_exists():
    with patch("src_generator.main.os.mkdir") as patched_mkdir:
        patched_mkdir.side_effect = FileExistsError
        main.create_ci_dirs()
        assert patched_mkdir.call_count == 1


def test_read_config_no_filters(test_filters, helpers):
    res: list[filters.Button] = main.read_config(helpers.get_test_files()[0])
    assert res == test_filters[0]


def test_read_config_one_filter(test_filters, helpers):
    res: list[filters.Button] = main.read_config(helpers.get_test_files()[1])
    assert res == test_filters[1]


def test_read_config_multiple_filters(test_filters, helpers):
    res: list[filters.Button] = main.read_config(helpers.get_test_files()[2])
    assert res == test_filters[2]


def test_generate_combinations_no_filters(test_filters):
    res: list[tuple[str, str]] = main.generate_combinations(test_filters[0])
    assert len(res) == 1
    assert res[0][1].endswith("\n\n")


def test_generate_combinations_one_filter(test_filters):
    res: list[tuple[str, str]] = main.generate_combinations(test_filters[1])
    assert len(res) == 2
    assert res[0][1].endswith("\n\n")
    assert res[1][1].endswith("\n\n##foo\n")


def test_generate_combinations_multiple_filters(test_filters):
    res: list[tuple[str, str]] = main.generate_combinations(test_filters[2])
    assert len(res) == 9
    assert res[8][1].endswith("\n\n##bar\n\n##baz\n")


def test_write_one_file(test_filters):
    with patch("builtins.open", mock_open()) as m:
        main.write_files(
            constants.pub_lists_dir, main.generate_combinations(test_filters[0])
        )
        assert m.call_count == 1
        assert constants.pub_lists_dir in m.call_args.args[0]


def test_write_multiple_files(test_filters):
    with patch("builtins.open", mock_open()) as m_open:
        main.write_files(
            constants.pub_lists_dir, main.generate_combinations(test_filters[2])
        )
        assert m_open.call_count == 9


def test_copy_webapp():
    with patch("src_generator.main.shutil.copytree") as mock_copy, patch(
        "src_generator.main.os.remove"
    ) as mock_rm:
        main.copy_webapp()
        mock_copy.assert_called_once_with(
            constants.webapp_dir, constants.pub_dir, dirs_exist_ok=True
        )
        mock_rm.assert_called_once_with(constants.webapp_node_file_dest)


def test_version_stamp_index():
    with patch("src_generator.main.subprocess.run") as mock_run:
        main.version_stamp_index()
        mock_run.assert_called_once_with(
            constants.version_stamp_cmd, shell=True, check=True
        )

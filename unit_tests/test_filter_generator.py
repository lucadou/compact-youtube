import pytest

from src_generator import filter_generator
from src_generator import filters


@pytest.fixture
def test_filters() -> tuple[filters.FilterInfo]:
    return (
        {"name": "Button1", "mode": "show_foo", "filter": "##btn1>bar"},
        {"name": "Button2", "mode": "show_bar", "filter": "##btn2>bar"},
    )


def test_generate_header_no_filter():
    res: tuple[str, str] = filter_generator.generate_header(())
    assert "Variant Configuration:\n!     \n! Expires" in res[1]


def test_generate_header_one_filter(test_filters):
    f_list: tuple[filters.FilterInfo, ...] = (test_filters[0],)
    res: tuple[str, str] = filter_generator.generate_header(f_list)
    assert "Button1=show_foo" in res[1]


def test_generate_header_multiple_filters(test_filters):
    f_list: tuple[filters.FilterInfo, ...] = test_filters
    res: tuple[str, str] = filter_generator.generate_header(f_list)
    assert "Button1=show_foo, Button2=show_bar" in res[1]


def test_generate_header_contains_version_stamp():
    res: tuple[str, str] = filter_generator.generate_header(())
    assert "! Version:" in res[1]


def test_generate_file_contents_no_filter():
    res: str = filter_generator.generate_file_contents(())
    assert res == ""


def test_generate_file_contents_one_filter(test_filters):
    f_list: tuple[filters.FilterInfo, ...] = (test_filters[0],)
    res: str = filter_generator.generate_file_contents(f_list)
    assert res == "##btn1>bar"


def test_generate_file_contents_multiple_filters(test_filters):
    res: str = filter_generator.generate_file_contents(test_filters)
    assert res == "##btn1>bar\n##btn2>bar"


def test_build_file_no_filter():
    res: tuple[str, str] = filter_generator.build_file(())
    assert res[1].endswith(".md\n\n")


def test_build_file_one_filter(test_filters):
    f_list: tuple[filters.FilterInfo, ...] = (test_filters[0],)
    res: tuple[str, str] = filter_generator.build_file(f_list)
    assert res[1].endswith("##btn1>bar")
    assert "Button1=show_foo" in res[1]


def test_build_file_multiple_filters(test_filters):
    res: tuple[str, str] = filter_generator.build_file(test_filters)
    res[1].endswith("##btn1>bar\n##btn2>bar")
    assert "Button1=show_foo, Button2=show_bar" in res[1]

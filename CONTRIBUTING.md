# Contributing

Compact YouTube welcomes your contributions.

If you would like to suggest features, report bugs, etc., feel free to open an
[Issue](https://gitlab.com/lucadou/compact-youtube/-/issues).

If you would like to help develop the filter list or its associated
applications, merge requests are welcome.
For major changes, please open an issue first to discuss what you would like
to change.

### Table of Contents

* [Contributor Code of Conduct](#contributor-code-of-conduct)
* [Issue-Writing Guidelines](#issue-writing-guidelines)
* [Environment Setup](#environment-setup)
* [Running the Application](#running-the-application)
  * [Building the Filter Lists](#building-the-filter-lists)
  * [Previewing the Results](#previewing-the-results)
  * [Using with a Dedicated Webserver](#using-with-a-dedicated-webserver)
* [Testing the Application](#testing-the-application)
  * [Evaluating Code Coverage](#evaluating-code-coverage)
* [Coding Style](#coding-style)
  * [JavaScript](#javascript)
  * [Python](#python)
* [Notes/FAQ](#notesfaq)

## Contributor Code of Conduct

If you are going to contribute to this project in any way (bug reports, merge
requests, etc.), please follow these guidelines:

* Keep things civil and respectful.
* Do not harass contributors, on- or off-site.

This project is purely a volunteer effort, so please be courteous.

## Issue-Writing Guidelines

Please follow these guidelines when reporting issues:

* Give your issue a clear, descriptive title.
* Be accurate and precise.
* If you have multiple problems, create a separate issue for each one.
* When available, use issue templates.

Reported issues not following these guidelines may be closed without warning.

## Environment Setup

If you wish to contribute to this project, you will need the following:

* Node LTS/Gallium:
  * Not sure what the minimum Node version is, Gallium was just the version
    used to develop this.
  * Only required for IDE hints; the website itself is deployed entirely
    statically.
  * Setup your development environment with the following commands:
    ```bash
    npm ci
    ```
* Python 3.8+:
  * Application was developed with 3.10 but 3.8 is the minimum required
    for some [Typing extensions](https://stackoverflow.com/a/39458225/15264046).
  * All application functionality is from the stdlib.
  * Formatting/linting rules are checked with flake8 and pylint (via Pipenv).
  * Setup your development environment with the following commands:
    ```bash
    python3 -m venv venv
    source venv/bin/activate
    pip install -r requirements/dev.txt  
    ```
  * When developing, it is recommended that you use
    [blackd](https://black.readthedocs.io/en/stable/usage_and_configuration/black_as_a_server.html)
    and an IDE plugin such as
    [BlackConnect](https://plugins.jetbrains.com/plugin/14321-blackconnect)
    to auto-format as you go.

## Running the Application

### Building the Filter Lists

```bash
python3 -m venv venv
source venv/bin/activate
cd src_generator/
python main.py
```

### Previewing the Results

After building the filter lists in the newly-generated `public` directory,
you can quickly launch a webserver to preview the results with:

```bash
cd public/
python -m http.server
```

This is a very simple, single-threaded webserver that can deadlock
when attempting to serve multiple sessions
and is not suitable for more than local testing.
If you want to serve these files publicly,
you will want to use a more appropriate application such as Nginx
(see [Using with a Dedicated Webserver](#using-with-a-dedicated-webserver)).

### Using with a Dedicated Webserver

Although this application has a GitLab CI file for automatic building and hosting on
[GitLab Pages](https://docs.gitlab.com/ee/user/project/pages/),
you may not wish to host there.

If you want to run this on your own server,
after building the filter lists in the newly-generated `public` directory,
you can install them in a webserver directory or set up your webserver to point to them.
This is an entirely static website, so it should work with any webserver.

A sample Nginx configuration file can be found in
[docs/nginx-service.conf](docs/nginx-service.conf).

## Testing the Application

This application uses Pytest,
and passing tests are a requirement for all merge requests.
To run tests locally:

```bash
# Within your venv
# Run individually with:
pytest unit_tests/
pytest integration_tests/
# Alternatively, to run them together:
pytest unit_tests/ integration_tests/
```

### Evaluating Code Coverage

The CI pipeline has been configured to require at least 90% code coverage.
To evaluate code coverage locally:

```bash
# Within your venv
coverage run --source=src_generator --omit=unit_tests,integration_tests -m pytest unit_tests/ integration_tests/
coverage html --fail-under=90 -d ./cover
```

## Coding Style

### JavaScript

* Variables must be declared using `let` or `const`.
  * No `var`, as it is scoped too broadly.
  * No implicit declarations, to avoid
    [The Horror of Implicit Globals](http://blog.niftysnippets.org/2008/03/horror-of-implicit-globals.html).
* Classes and methods must use [JSDoc docstrings](https://jsdoc.app/)
* Source files must have a copyright and GPL header at the top.
  * JS files must also have
    [LibreJS tags](https://www.gnu.org/software/librejs/free-your-javascript.html#magnet-link-license).
* Variable names must be descriptive and use lowerCamelCase.
* Follow the
  [MDN styling guidelines](https://developer.mozilla.org/en-US/docs/MDN/Writing_guidelines/Writing_style_guide/Code_style_guide/JavaScript).

### Python

* Code must pass black, flake8, and pylint with no errors or warnings.
  * You can check your code using the following steps:
    ```bash
    source venv/bin/activate
    cd src_generator/
    flake8 .
    pylint .
    black --check .
    ```
  * The following exceptions are allowed:
    * Conflicts between the tools - black generally has the best formatting,
      so when conflicts arise (e.g. line length),
      adjust them to favor black's style unless there is a pressing reason not to.
    * Unavoidably long lines (e.g. links in comments).
      * To get flake8 to ignore it, append this to the end of the line:
        ```python
        # noqa: E501
        ```
    * Import errors caused by importing other classes in the project
      (and which work just fine when the program is run).
      * To get pylint to ignore it, append this to the end of the line:
        ```python
        # pylint: disable=import-error
        ```
* *Always* use [type hints](https://docs.python.org/3/library/typing.html),
  and make them as detailed as you can.
  * This makes IDE type checking more accurate and makes the code more
    maintainable.
  * When using dicts with non-trivial structures or importing
    configuration files (e.g. JSON), TypedDicts are mandatory.
  * The only time it is acceptable to not use detailed type hints is if you are
    interacting with a library that constructs specific types on the fly
    (e.g. boto3 for AWS has some classes only available at runtime which cause
    errors when trying to create type hints for them).
* Imports should not be compacted (i.e. no `from X import Y` or `import X.Y`).
  * There are several exceptions to this rule:
    * Type hints (e.g. `from typing import Tuple` is allowed).
    * Importing another file from within the project hierarchy.
      * This is because relative imports in Python are a nightmare - sometimes
        they work perfectly and `from X import Y` is broken, other times it's
        the reverse.
    * Classes which misbehave if you do not do `from X import Y`
      (e.g. some Flask and Flask-Babel classe).
    * Classes which are not being used for typing.
  * The reason for this is because, when using type hints, using compacted
    imports can confuse IDEs, especially for more detailed type hints.
    * This does mean the code is less compact (less "Pythonic", even), but the
      benefits outweigh the potential annoyance of longer lines.
  * Example:
    ```python
    # Do not do this
    from pathlib import Path
    bad_path: Path = Path('...')
    # Instead, do this
    import pathlib
    good_path: pathlib.Path = pathlib.Path('...')
    ```
* Do not use magic numbers/values - instead, unless it's for logging, it should
  be defined in a constants file.
* Log liberally.
* Use f-strings when you need to do string interpolation.
  * Exception: logging statements should use `%s`
    * https://stackoverflow.com/a/54368109
    * https://docs.python.org/3/howto/logging.html#optimization
  * Example:
    ```python
    i: int = 123
    print(f'{i} * 2 = {i * 2}') # => '123 * 2 = 246'
    logger.info('%i * 2 = %i', i, (i * 2)) # => '123 * 2 = 246'
    ```
* Classes and methods must use
  [reStructuredText docstrings](http://daouzli.com/blog/docstring.html#restructuredtext).
  * All classes and methods must provide a detailed description of what they
    do, along with any input value restrictions, implicit assumptions, and
    error cases.
  * If a return type is non-trivial, specific examples must be given showing
    the results of various input combinations.
  * Exceptions that may be raised/passed back to the caller must be detailed
    using this syntax:
    ```python
    :raise ExceptionType: exception description and how it is triggered
    ```
  * See also: [reST Sphinx documentation](https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html)
* Source files must have a copyright and GPL header at the top.
* Variable names must be descriptive and use snake_case.

## Notes/FAQ

> When attempting to run the tests, I get an error like this:
> ```python
> test_main.py:9 (test_create_ci_dirs)
> def test_create_ci_dirs():
> >       with patch("main.os") as patched_os:
> 
> test_main.py:11: 
> _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
> /usr/lib/python3.10/unittest/mock.py:1431: in __enter__
>     self.target = self.getter()
> /usr/lib/python3.10/unittest/mock.py:1618: in <lambda>
>     getter = lambda: _importer(target)
> _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ 
> 
> target = 'main'
> 
>     def _importer(target):
>         components = target.split('.')
>         import_path = components.pop(0)
> >       thing = __import__(import_path)
> E       ModuleNotFoundError: No module named 'main'
> 
> /usr/lib/python3.10/unittest/mock.py:1257: ModuleNotFoundError
> ```

If you are using IntelliJ IDEA, PyCharm, or another JetBrains IDE,
right-click on the [src_generator](src_generator) folder,
go to "Mark Directory as", then click on "Sources Root".

Somehow, that fixes the import paths for the patcher.

If you're using anything else,
feel free to expand this answer when you figure it out.

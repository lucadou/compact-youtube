# Changelog
All notable changes to this project will be documented in this file.

Maintainer Notes:

- If the buttons change, make sure to update the web selector application form.
- Adding/removing buttons on the form constitutes a breaking change.
  - All breaking changes *must* result in a new major version since they eliminate
    backwards compatibility with all previous filter lists.

## [4.0.3] - 2024-05-04

### Changed

- Adjust screenshot width constraints to be based on device width rather than static

## [4.0.2] - 2024-05-04

### Added

- Before/after images to website (#23)

## [4.0.1] - 2024-05-04

### Changed

- Fixed import errors in CI (#21)
- Strip version info from pre-generated baseline test comparison files (#22)

## [4.0.0] - 2024-05-04

### Added

- Integration tests (#19)
- "Join" and "Subscribe" button filter options (#16)
- JPEG-XL versions of screenshots (#15)
- Unit tests (#2)
- Version stamp in footer and filter lists (#18)

### Changed

- Replaced screenshots with higher resolution versions (#13)

### Removed

- Dislike button "Show button only" option (#17)
- Filters for "Description" button (#20)

## [3.3.0] - 2024-04-30

### Added

- Black code formatting & CI (#12)
- Instructions for dedicated hosting & sample Nginx configuration file (#14)
- Link to TRCH (#7)

### Changed

- Changed application domain (switched to custom domain for GitLab Pages, #9)
- Fixed issue templates being unusable (#10)
- Switched from pipenv to venv (#11)
- Updated copyright year
- Updated CSS selectors to take into account YouTube page layout updates (#6)

## [3.2.0] - 2023-07-02

### Added

- Second example comparison image in readme

### Changed

- Updated copyright year
- Updated CSS selectors to take into account YouTube page layout updates (#5)

## [3.1.0] - 2022-10-28

### Added

- Feature request template (part of #3)
- Issue template (part of #3)
- Right-click to add to uBlock instructions (part of #4)

### Changed

- Updated readme with new instructions for adding filter lists (part of #4)

## [3.0.7] - 2022-10-26

### Changed

- Fixed CI bug trying to call "python" in release stage

## [3.0.6] - 2022-10-26

### Added

- Contributions guide, including coding standards and environment setup steps
- flake8 and pylint to pipenv
- Linting stage in CI pipeline
- Linting attestation in MR template

### Changed

- Modified header-levels on MR template
- Renamed some directories
- Updated table of contents in readme
- Updated link to readme setup section on website
- Webpage theme color

## [3.0.5] - 2022-10-24

### Added

- GitLab merge request template

## [3.0.4] - 2022-10-24

### Added

- Releases section to GitLab CI

## [3.0.3] - 2022-10-24

### Added

- License notice to every source file, per GPL recommendations

## [3.0.2] - 2022-10-24

### Changed

- Modified filter file header format
- Updated readme with new instructions on how to use the website

## [3.0.1] - 2022-10-23

### Changed

- Fixed webapp files not being copied over properly

## [3.0.0] - 2022-10-23

### Added

- Application to auto-generate all possible configuration files (#1)
- Web interface to find the configuration file for you (#1)

## [2.0.0] - 2022-10-23

### Added

- Instructions on how to use the blocklist in the readme

### Changed

- Filter list now structured better for importing via URL

## [1.1.0] - 2022-10-23

### Added

- Changelog
- Git repo
- License
- Readme

### Changed

- Updated filters for new YouTube UI layout

## [1.0.1] - 2022-05-13

### Added

- Download button filters

## [1.0.0] - 2022-05-06

### Added

- Filter list

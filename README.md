# Compact YouTube

uBlock filter lists that makes the YouTube interface cleaner, more compact,
and usable for smaller window sizes, built to your specification.

This is accomplished by removing the text from buttons underneath the video
player and removing redundant and unnecessary buttons (the Description button,
Download button for non-Premium users, etc.).

![A before and after comparison of the buttons beneath a YouTube video showing the buttons overflowing off the screen in the "before" section and fitting nicely on the same line as the subscriber count in the "after" section](docs/imgs/img2-comparison-1.png "Normal YouTube UI vs. the compacted UI, example 1")

![A before and after comparison of the buttons beneath a YouTube video showing the buttons overflowing off the screen in the "before" section and fitting nicely on one line in the "after" section](docs/imgs/img3-comparison-2.png "Normal YouTube UI vs. the compacted UI, example 2")

*(Videos featured above:
[LGR - "It's Time to Talk About My Synth Setup"](https://www.youtube.com/watch?v=UgNqzbcr2Pw) &
[Ri Archives - "Crystals - Alan Holden 1958"](https://www.youtube.com/watch?v=Wp6bN9vN6e4))*

### Table of Contents

* [Getting Started](#getting-started)
  * [Requirements](#requirements)
  * [Installation](#installation)
    * [uBlock Integration](#ublock-integration-recommended)
    * [Manual Setup](#manual-setup-advanced-users)
* [Notes/FAQ](#notesfaq)
* [Versioning](#versioning)
* [Contributing](#contributing)
* [Authors](#authors)
* [License](#license)

## Getting Started

### Requirements

* A modern browser with the [uBlock Origin](https://ublockorigin.com/)
  extension installed.

### Installation

1. Go to the [Filter List Generator](https://compact-youtube.apps.lucadou.sh/).
2. Fill out the form according to your preferences.
3. Click the "Generate Filter List" button.
4. Pick your method:
   a. [uBlock Integration](#ublock-integration-recommended) (recommended method)
   b. [Manual](#manual-setup-advanced-users) (for advanced users)

#### uBlock Integration (Recommended)

1. Right-click on the link provided.
2. Go to the "uBlock" or "uBlock Origin" sub-menu.
3. Inside the sub-menu, click on "Subscribe to filter list...".
4. A new tab showing the contents of the filter list will open;
   switch to it if your browser does not automatically do so.
5. Click on the "Subscribe" button in the top right corner of the
   uBlock Asset Viewer tab.

#### Manual Setup (Advanced Users)

1. Click on the "Manual (Advanced)" option.
2. Copy the generated link to your clipboard.
3. Open your uBlock Dashboard.
4. Go to the "Filter lists" tab.
5. Scroll down to the bottom.
6. Check the box next to "Import...".
7. In the box that appears, paste the link you previously copied.
8. Press the "Apply Changes" button at the top of the page.
9. Press the "Update Now" button at the top of the page.

*(Manual method instructions were based on
https://github.com/gorhill/uBlock/wiki/Filter-lists-from-around-the-web)*

## Notes/FAQ

> It no longer works, how do I fix it?

1. [Purge your uBlock caches.](https://github.com/gorhill/uBlock/wiki/Dashboard:-Filter-lists#purge-all-caches)
2. [Update your uBlock filters.](https://github.com/gorhill/uBlock/wiki/Dashboard:-Filter-lists#update-now)

> I did the above steps and it still isn't working

1. Try re-doing the steps in [Installation](#installation),
   as YouTube may have added more buttons and your previous configuration
   may no longer be available as a result.
   1. An easy way to tell when this has occurred is when the version in your
      filter list does not match the version in the footer of the website.
   2. Alternatively, if your filter list has no version number,
      then it pre-dates the version stamping introduced in 4.0.0,
      in which case you definitely need to redo the steps above,
      as breaking changes have occurred since you generated your filter list.
   3. Breaking changes will always be indicated by a new major version
      (e.g. 3.9.9 -> 4.0.0).
2. If that does not work, chances are, YouTube has changed things up yet again. 
   Feel free to open an [Issue](https://gitlab.com/lucadou/compact-youtube/-/issues)
   to let us know they've broken things.

> Why do some buttons only have two options instead of three?

This is because of how YouTube has changed the interface over the years:

* The Dislike button used to have a counter, but now there is no text beside it,
* but since YouTube may re-introduce text in the future, the options were reduced to
  "Show icon and text" and "Hide icon and text".
* The Subscribe button always has text but only has an icon if you are subscribed,
  so it was changed to only have "Show icon and text" and "Hide icon and text".
* The Join button was a later introduction and has no icon, but since it may in the future,
  it was changed to only have "Show icon and text" and "Hide icon and text".

> Should I use this if I or someone I share my computer with uses a
> screen reader?

Probably not.
We do not know how uBlock Origin affects the functionality of screen readers and
other accessibility software, but we can only assume it would negatively impact
them.

That said, if you use different user accounts or browsers/browser profiles,
you're fine.

## Versioning

This project uses Semantic Versioning, aka
[SemVer](https://semver.org/spec/v2.0.0.html).

## Contributing

To learn more about contributing, including development environment setup
and code style guidelines, see the [CONTRIBUTING](CONTRIBUTING.md)
file for details.

## Authors

* [Luna Lucadou](https://luna.lucadou.sh/) - Wrote the application, website,
  and filters in her free time.

## License

This project is licensed under the GNU General Public License v3.0 or any
later version - see the [LICENSE](LICENSE.md) file for details.

# Version: [version number]

## Notable Changes:

* Item1
* Item2
* Item3

### Explanations of Changes (if Needed):



## Related Issues and Merge Requests

*(You can delete any lines that do not apply.
If no lines apply, you can delete this section.)*

* This MR closes #X, #Y, #Z
* This MR is part of #X, #Y, #Z
* This MR is related to !X, !Y, !Z

## Merge Checklist:

- [ ] Changelog entry
- [ ] Changes tested (if needed)
- [ ] Documentation updated (if needed)
- [ ] Readme updated (if needed)
- [ ] Version stamp updated in the [constants file](src_generator/constants.py)
  (guidance on versioning can be found in the [changelog](CHANGELOG.md))
- [ ] CI passed on final commit prior to merge
- [ ] Unnecessary git tags for previous commits in this MR removed
      (`git tag -d vX.Y.Z`, `git push --delete origin vX.Y.Z`)
- [ ] Git tag created for final commit prior to merge (`git tag vX.Y.Z`)
- [ ] Git tag pushed (`git push origin vX.Y.Z`)
# Prerequisites

Please answer the following questions for yourself before submitting an issue.

- [ ] (If maintaining your own instance, otherwise ignore)
      I am running the latest version
- [ ] I checked the documentation and found no answer
- [ ] I checked to make sure that this issue has not already been filed

# Expected Behavior

Please describe the behavior you are expecting

# Current Behavior

What is the current behavior?

## Steps to Reproduce

Please provide detailed steps for reproducing the issue.

1. Step 1
2. Step 2
3. etc.

## Context

Please provide any relevant information about your setup.
This is important in case the issue is not reproducible except under
certain conditions.

* Operating System Name:
* Operating System Version:
* Browser Name:
* Browser Version:
* uBlock Version:

# Relevant Logs

Please include any relevant log snippets or files here.
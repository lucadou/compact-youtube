/**
 * This file is part of Compact YouTube, a set of filter lists for uBlock.
 * Copyright (C) 2022-2024 Luna Lucadou
 *
 * Compact YouTube is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Compact YouTube is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Compact YouTube.
 * If not, see <https://www.gnu.org/licenses/>.
 */
// LibreJS license indicator
// https://www.gnu.org/software/librejs/manual/html_node/Setting-Your-JavaScript-Free.html#License-tags
// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

'use strict';

//const bootstrap = require('bootstrap');
const fs = require('fs');
const http = require('http');
const md5 = require("crypto-js/md5");
const url = require('url');
const cType = {'Content-Type': 'text/html'}

http.createServer(function (req, res) {
    let urlInfo = url.parse(req.url, true);
    let urlPath = '';

    if (urlInfo.pathname === '/') {
        urlPath = 'index.html';
    } else {
        urlPath = urlInfo.pathname;
    }

    fs.readFile(urlPath, function(err, data) {
        if (err) {
            res.writeHead(404, cType);
            return res.end('404 Not Found');
        }
        res.writeHead(200, cType);
        res.write(data);
        return res.end();
    });
}).listen(5000);
// @license-end

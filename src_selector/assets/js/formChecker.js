/**
 * This file is part of Compact YouTube, a set of filter lists for uBlock.
 * Copyright (C) 2022-2024 Luna Lucadou
 *
 * Compact YouTube is free software: you can redistribute it and/or modify it
 * under the terms of the GNU General Public License as published by the Free
 * Software Foundation, either version 3 of the License, or (at your option) any
 * later version.
 *
 * Compact YouTube is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Compact YouTube.
 * If not, see <https://www.gnu.org/licenses/>.
 */
// LibreJS license indicator
// https://www.gnu.org/software/librejs/manual/html_node/Setting-Your-JavaScript-Free.html#License-tags
// @license magnet:?xt=urn:btih:1f739d935676111cfff4b4693e3816e664797050&dn=gpl-3.0.txt GPL-v3-or-Later

'use strict';

/**
 * @typedef {Object} FormContents
 * @property {String} DislikeButton - the radio button selected for the Dislike button
 * @property {String} ShareButton - the radio button selected for the Share button
 * @property {String} DownloadButton - the radio button selected for the Download button
 * @property {String} ThanksButton - the radio button selected for the Thanks button
 * @property {String} ClipButton - the radio button selected for the Clip button
 * @property {String} SaveButton - the radio button selected for the Save (to Playlist) button
 * @property {String} DescriptionButton - the radio button selected for the Description button
 */
/**
 * Reads the contents of the form and returns them to you as a FormContents Object
 *
 * @returns {FormContents} - the contents of the form
 */
function getFormSelections() {
    return {
        'DislikeButton': document.forms.filterForm.DislikeButton.value,
        'ShareButton': document.forms.filterForm.ShareButton.value,
        'DownloadButton': document.forms.filterForm.DownloadButton.value,
        'ThanksButton': document.forms.filterForm.ThanksButton.value,
        'ClipButton': document.forms.filterForm.ClipButton.value,
        'SaveButton': document.forms.filterForm.SaveButton.value,
        'SubscribeButton': document.forms.filterForm.SubscribeButton.value,
        'JoinButton': document.forms.filterForm.JoinButton.value
    };
}

/**
 * When the form is submitted, reads in all its contents and generates a link
 * to the user's requested configuration.
 * Then, it inserts the link into a modal which it makes visible to the user.
 *
 * @param {Event} event - the event that triggered this function call
 */
function processForm(event) {
    event.preventDefault();

    // Get form selections
    const formSelections = getFormSelections();
    // Convert to form "name=mode, ..."
    let formValues = [];
    for (const [name, mode] of Object.entries(formSelections)) {
        formValues.push(name + '=' + mode);
    }
    let formValuesString = formValues.join(', ');
    // MD5 hash selections
    let filterMD5 = CryptoJS.MD5(formValuesString).toString();

    /* Generate links for user
     * "/lists/" is added to ensure window.location.href not containing an
     * ending '/' does not cause problems, and then we call replaceAll to
     * de-duplicate '/'s
     */
    let filterURL = window.location.protocol + '//' +
        (window.location.host + window.location.pathname + '/lists/' + filterMD5
        + '.txt').replaceAll('//', '/');
    let easyAddURL = 'https://subscribe.adblockplus.org/?location=' + filterURL +
        '&title=Compact%20YouTube%20%28Variant%20' + filterMD5 + '%29';

    // Post links on page
    document.getElementById('filterURLEasyAdd').innerHTML =
        '<a href="' + easyAddURL + '">' + easyAddURL + '</a>';
    document.getElementById('filterURLAdvanced').innerHTML =
        '<a href="' + filterURL + '">' + filterURL + '</a>';
}

window.addEventListener('load', function () {
    // Enable tooltips
    // https://getbootstrap.com/docs/5.2/components/tooltips/#enable-tooltips
    const tooltipTriggerList = document.querySelectorAll('[data-bs-toggle="tooltip"]');
    const tooltipList = [...tooltipTriggerList].map(tooltipTriggerEl => new bootstrap.Tooltip(tooltipTriggerEl));

    const formBtn = document.getElementById('formSubmit');
    formBtn.addEventListener('click', processForm, true);
    console.log('Loaded JS');
});
// @license-end
